# Enhancing Projected Augmented Reality Interactions using Haptic Technology

Student: Nicolas Legrand

Student number: 01710212

Supervisors: Supervisors: Prof. dr. ir. Filip De Turck, Dr. ir. Femke De Backere

Counsellors: Ir. Joris Heyse, Dr. ir. Maria Torres Vega, Ir. Stéphanie Carlier

## Objective measurements: Jupyter Notebooks

This repository contains the Jupyter notebooks used for the evaluation described in the paper. 
The implementation source code can be found in the following [git repository](https://gitlab.com/nicolaslegrand009/thesis_project).

### Requirements

Install the required python packages by using the following command in the root folder:

`python -m pip install -r requirements.txt`

It is advised to use [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) to create an isolated environment. 

A few handy conda commands are listed below:

- Create a conda environment
`conda create --name <environment-name> python=<3.7.6>`

- Use an exported environment file
`conda env create -f <environment-name>.yml`

### Data
The raw data can be found in the following [Google Drive folder](https://drive.google.com/drive/folders/1xye8VNUnVYo58M8ZktlB3U2x2HqghGwL?usp=sharing).

Set the downloaded data folder path in the "config.py" file.
Set the output folder path in the "config.py" file.

### Usage
`python -m notebook <any_notebook>`
