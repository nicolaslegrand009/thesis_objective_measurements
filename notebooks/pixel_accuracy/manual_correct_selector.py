import cv2
import os
from pathlib import Path
from shutil import copyfile
import dill

input_folder = f"N:\\GDrive\\MasterProef\\objective_tests\\data\\pixel_accuracy\\1.raw_saved_data\\markerless\\two_finger"
output_folder = f"N:\\GDrive\\MasterProef\\objective_tests\\data\\pixel_accuracy\\2.correct_data\\markerless\\two"

for filename in os.listdir(input_folder):
    if filename.endswith(".png"):
        id = Path(filename).stem
        img = cv2.imread(f"{input_folder}/{filename}", cv2.IMREAD_COLOR)
        print(filename)

        with open(f"{input_folder}/{id}.pkl", "rb") as f:
            x = dill.load(f)
            for detected in x:
                tip = detected["tip"]
                palm = detected["palm"]
                print(palm)
                cv2.circle(img,tuple(tip),5,(0,255,255),2)

        cv2.imshow("img", img)
        key = cv2.waitKey(0)

        if key == ord('y'):

            copyfile(f"{input_folder}/{id}.pkl",f"{output_folder}/{id}.pkl")
            copyfile(f"{input_folder}/{id}.png",f"{output_folder}/{id}.png")


