import os
from pathlib import Path
import numpy as np
import cv2
from shutil import copyfile
import dill

input_folder = f"N:\\GDrive\\MasterProef\\objective_tests\\data\\pixel_accuracy\\2.correct_data\\markerbased\\two"
output_folder = f"N:\\GDrive\\MasterProef\\objective_tests\\data\\pixel_accuracy\\3.annotated_data\\markerbased\\two"

file_count = len(
    [name for name in os.listdir(input_folder) if os.path.isfile(f"{input_folder}/{name}") and name.endswith(".png")])
print(f"file_count:{file_count}")
skip = file_count / 30
print(f"skip:{skip}")

files = [name for name in os.listdir(input_folder) if name.endswith(".png")]
np.random.shuffle(files)
selected = files[0:30]

print("selected count:" + str(len(selected)))

img = None

mouseX, mouseY = None, None
mouseXX, mouseYY = None, None


def event_listen(event, x, y, flags, param):
    global mouseXX, mouseYY
    global mouseX, mouseY

    if event == cv2.EVENT_LBUTTONDBLCLK:
        mouseXX, mouseYY = x, y
    if event == cv2.EVENT_RBUTTONDBLCLK:
        mouseX, mouseY = x, y


for filename in selected:
    if filename.endswith(".png"):
        id = Path(filename).stem
        img = cv2.imread(f"{input_folder}/{filename}")
        cv2.imshow("img", img)
        cv2.setMouseCallback('img', event_listen)

        key = cv2.waitKey(0)
        print(str((mouseX, mouseY)))
        print(str((mouseXX, mouseYY)))
        print()

        copyfile(f"{input_folder}/{id}.pkl", f"{output_folder}/{id}.pkl")
        copyfile(f"{input_folder}/{id}.png", f"{output_folder}/{id}.png")

        with open(f"{output_folder}/{id}.truth", "wb") as output_file:
            dill.dump([(mouseX, mouseY), (mouseXX, mouseYY)], output_file)
